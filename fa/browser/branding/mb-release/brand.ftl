# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = مرورگر Mullvad
-brand-short-name = مرورگر Mullvad
-brand-full-name = مرورگر Mullvad
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = مرورگر Mullvad
-vendor-short-name = Mullvad
trademarkInfo = مرورگر Mullvad و لوگوهای مرورگر Mullvad علائم تجاری Mullvad VPN AB هستند.
