# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } on yksityisyyteen keskittyvä selain, jonka ovat kehittäneet <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> ja <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label> yhteistyössä. Selain minimoi seurannan ja estää laitteen yksilölliseen tunnistamiseen tähtäävien fingerprinting-tekniikoiden käytön.
mullvad-about-readMore = Lue lisää
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Tuki ja palaute: { $emailAddress }
mullvad-about-telemetryLink = Telemetria

## Mullvad browser home page.

about-mullvad-browser-developed-by = <a data-l10n-name="tor-project-link">Tor Projectin</a> ja <a data-l10n-name="mullvad-vpn-link">Mullvad VPN:n</a> yhteistyössä kehittämä
about-mullvad-browser-use-vpn = Käytä <a data-l10n-name="with-vpn-link">Mullvad VPN</a>:ää selaimen kanssa ja nauti vieläkin yksityisemmästä verkkoelämästä.
about-mullvad-browser-learn-more = Haluatko tietää lisää selaimesta? <a data-l10n-name="learn-more-link">Hyppää myyränkoloon</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } | Etusivu

## about:rights page.

rights-mullvad-intro = { -brand-short-name } on ilmainen avoimen lähdekoodin ohjelmisto.
rights-mullvad-you-should-know = Pari tärkeää juttua tiedoksesi:
rights-mullvad-trademarks = Sinulle ei myönnetä tavaramerkkioikeuksia tai lisenssejä tavaramerkkeihin, jotka { -brand-short-name } tai jokin muu osapuoli omistaa. Tämä koskee rajoituksitta myös { -brand-short-name }-logoa sekä tuotteen nimeä.

## about:telemetry page.

telemetry-title = Telemetriatiedot
telemetry-description = { -brand-short-name } ei käytä telemetriaa.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
