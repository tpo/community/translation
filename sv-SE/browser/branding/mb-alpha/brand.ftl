# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = Mullvad Browser
-brand-short-name = Mullvad Browser Alpha
-brand-full-name = Mullvad Browser Alpha
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Mullvad-webbläsare
-vendor-short-name = Mullvad
trademarkInfo = Mullvad-webbläsare och logotyperna för Mullvad-webbläsare är varumärken som tillhör Mullvad VPN AB.
