# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = Mullvad ブラウザ
-brand-short-name = Mullvad ブラウザ
-brand-full-name = Mullvad ブラウザ
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Mullvad ブラウザー
-vendor-short-name = Mullvad
trademarkInfo = Mullvad ブラウザーおよび Mullvad ブラウザーのロゴマークは Mullvad VPN AB の登録商標です。
