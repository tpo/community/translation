# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name }  is een op privacy gerichte webbrowser die is ontwikkeld in samenwerking tussen <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> en het <label data-l10n-name="mullvad-about-torProjectLink">Tor-project</label>. De browser is gemaakt om tracking en fingerprinting tot een minimum te beperken.
mullvad-about-readMore = Meer lezen
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetrie

## Mullvad browser home page.

about-mullvad-browser-developed-by = Ontwikkeld in samenwerking tussen het <a data-l10n-name="tor-project-link">Tor-project</a> en <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Krijg meer privacy door de browser <a data-l10n-name="with-vpn-link">met Mullvad VPN</a> te gebruiken.
about-mullvad-browser-learn-more = Nieuwsgierig naar meer informatie over de browser? <a data-l10n-name="learn-more-link">Duik in de molshoop</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } Start

## about:rights page.

rights-mullvad-intro = { -brand-short-name } is gratis opensourcesoftware.
rights-mullvad-you-should-know = Er zijn enkele dingen die u moet weten:
rights-mullvad-trademarks =
    Er worden u geen handelsmerkrechten of licenties toegekend voor de handelsmerken van
    de { -brand-short-name } of enige partij, inclusief maar niet beperkt tot de naam of het logo van de { -brand-short-name }.

## about:telemetry page.

telemetry-title = Informatie over telemetrie
telemetry-description = Telemetrie is uitgeschakeld in { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
