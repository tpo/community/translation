# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } er en personvernfokusert nettleser utviklet i samarbeid med <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> og <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. Den er produsert for å minimalisere sporing og fingeravtykk på nettet.
mullvad-about-readMore = Les mer
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetri

## Mullvad browser home page.

about-mullvad-browser-developed-by = Utviklet i samarbeid med <a data-l10n-name="tor-project-link">Tor Project</a> og <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Få mer personvern ved å bruke nettleseren <a data-l10n-name="with-vpn-link">med Mullvad VPN</a>.
about-mullvad-browser-learn-more = Vil du vite mer om nettleseren? <a data-l10n-name="learn-more-link">Se nærmere på muldvarphullet</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name }-startside

## about:rights page.

rights-mullvad-intro = { -brand-short-name } er en gratis programvare med åpen kildekode.
rights-mullvad-you-should-know = Det er noen få ting du bør vite:
rights-mullvad-trademarks =
    Du er ikke gitt noen varemerkerettigheter eller lisenser til varemerkene til
    { -brand-short-name } eller en hvilken som helst part, inkludert, men ikke begrenset til
    { -brand-short-name }s navn eller logo.

## about:telemetry page.

telemetry-title = Informasjon om telemetri
telemetry-description = Telemetri er deaktivert i { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
